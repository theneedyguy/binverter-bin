package main

import (
	"gitlab.com/theneedyguy/binverter-go"
	"flag"
	"fmt"
	"os"
)

var (
	inputPath string
	outputPath string
	version = "1.1"
)

func init() {
	flag.StringVar(&inputPath, "if", "", "Input path to file")
	flag.StringVar(&outputPath, "of", "", "Outputpath to file")
}

func invert(inputPath string, outputPath string) {
	b := binverter.BinaryFile{InputPath: inputPath, OutputPath: outputPath}
	b.InvertFile()
}



func main() {
	versionPtr := flag.Bool("v", false, "Show version")
	flag.Parse()
	if *versionPtr {
		fmt.Println("Binverter Version:", version)
		os.Exit(1)
	}
	invert(inputPath, outputPath)
}
