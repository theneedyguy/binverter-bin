# binverter

This application is used to invert files on a binary level. Simply provide an input and an output and let the program invert the file.

## Usage

Execute the application as follows to start inverting files:

```
./binverter -if "path/to/input" -of "/path/to/output"
```

## Notes

- This is the binary version of the library **binverter-go** written by myself. If you want to learn more about the library used to make binverter you can check it out [here (gitlab)](https://gitlab.com/theneedyguy/binverter-go).
For the documentation, click [here (godoc)](https://godoc.org/gitlab.com/theneedyguy/binverter-go)
