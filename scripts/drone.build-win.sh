#!/bin/sh
REPO="gitlab.com/theneedyguy/binverter-bin"

go get ${REPO}
go build -a -v -installsuffix cgo -o release/${GOOS}/${GOARCH}/binverter-win-amd64.exe ${REPO}
sha512sum release/${GOOS}/${GOARCH}/binverter-win-amd64.exe > release/${GOOS}/${GOARCH}/${GOOS}-${GOARCH}-sha512sum.txt 
